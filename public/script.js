var Tempcanvas,Tempctx,canvas,ctx,mousePrevX,mousePrevY,touchPrevX,touchPrevY,touchX,touchY,startX,startY,endX,endY;    
var mouseX,mouseY,mouseDown=0;
var Color = "black";
var Size = "2";
var Tool = "pen";
var Fill = "empty";
var Back = "BackWhite";
document.getElementById('tools').style.display = "none";
init();
backColor();


function showtools(){
if (document.getElementById('HideTools').checked){document.getElementById('tools').style.display = "block";}
    else{document.getElementById('tools').style.display = "none";};}

function backColor() {
    ctx.beginPath();
    if (Back == "BackBlack"){ctx.fillStyle = "black";};
    if (Back == "BackWhite"){ctx.fillStyle = "white";};
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.closePath();
    ctx.fill();}

function draw(Prevx,Prevy,x,y) {
    if (Tool =='pen') {
    ctx.strokeStyle = Color;
    ctx.lineWidth = Size;
    ctx.beginPath();
    ctx.moveTo(Prevx,Prevy);
    ctx.lineTo(x, y);
    ctx.stroke();
    ctx.closePath();
    ctx.fill();};
    
    if (Tool =='eraser') {
    ctx.beginPath();
    if (Back == "BackBlack"){ctx.fillStyle = "black";ctx.fillRect(x,y,Size*6,Size*6);}    
    else {ctx.clearRect(x,y,Size*6,Size*6);};
    ctx.closePath();
    ctx.fill();}
    
    }
    
function Start(){startX = mousePrevX;startY = mousePrevY;}
function StartTouch(){startX = touchPrevX;startY = touchPrevY;}

function Shapes(x,y,ctx){

    if (Tool =='image') {
    var imag = new Image();        
    var file = document.getElementById('fichierImage').files[0];
    var reader = new FileReader();
    reader.addEventListener("load", function() {
    imag.src = reader.result;}, false);
    if (file) {reader.readAsDataURL(file);}
    imag.onload = function() { 
    var ratio = imag.width/imag.height;
    ctx.drawImage(imag,startX,startY,x-startX,y-startY);};}

    if (Tool =='line'){
    ctx.strokeStyle = Color;
    ctx.lineWidth = Size;
    ctx.beginPath();
    ctx.moveTo(startX,startY);
    ctx.lineTo(x,y);
    ctx.stroke();
    ctx.closePath();
    ctx.fill();}
    
    if (Tool =='square'){
    ctx.strokeStyle = Color;
    ctx.lineWidth = Size;
    ctx.beginPath();
    if (Fill == "empty") {ctx.strokeRect(startX,startY,x-startX,y-startY);}
    else {ctx.rect(startX,startY,x-startX,y-startY);}
    ctx.stroke();
    ctx.fillStyle = Color;
    ctx.fill();}
 

    if (Tool =='cercle'){
	ctx.beginPath();
	ctx.strokeStyle = Color;
	ctx.lineWidth = Size;
	ctx.arc(startX,startY, Math.hypot(x-startX,y-startY), 0, 2 * Math.PI);
	ctx.stroke();
	if (Fill == 'full') {
	ctx.fillStyle = Color;
	ctx.fill();}
	ctx.closePath();};
	
	if (Tool =='texte') {				
	var txtSize = Size*6;
	var txt = document.getElementById("text").value;
	ctx.font = txtSize + 'px serif';
	ctx.fillStyle = Color;
	ctx.fillText(txt,x,y);};
	}
 
function clearCanvas() {ctx.clearRect(0, 0, canvas.width, canvas.height);Tempctx.clearRect(0, 0,Tempcanvas.width,Tempcanvas.height);}

// Keep track of the mouse button being pressed and draw a dot at current location
function sketchpad_mouseDown() {
    mouseDown=1;
    Start();
    Push();
    }

// Keep track of the mouse button being released 
function sketchpad_mouseUp() {mouseDown=0;Shapes(mouseX,mouseY,ctx);} 

// Keep track of the mouse position
 function sketchpad_mouseMove(e) {
    getMousePos(e); 
    if (mouseDown==1) {
    Tempctx.clearRect(0, 0, Tempcanvas.width, Tempcanvas.height);
    Shapes(mouseX,mouseY,Tempctx);
    draw(mousePrevX,mousePrevY,mouseX,mouseY);}}

 // Get the current mouse position relative to the top-left of the canvas 
function getMousePos(e) {
    if (!e) var e = event; 
    if (e.offsetX) { 
        mousePrevX = mouseX;
        mousePrevY = mouseY;
        mouseX = e.offsetX; mouseY = e.offsetY; }
    else if (e.layerX) {
        mousePrevX = mouseX;
        mousePrevY = mouseY;
        mouseX = e.layerX; mouseY = e.layerY; } }
 
 // Draw something when a touch start is detected
function sketchpad_touchStart() {
    getTouchPos();    
    touchPrevX = touchX;
    touchPrevY = touchY;
    StartTouch();
    Push();
    event.preventDefault(); }
 
// Draw something and prevent the default scrolling when touch movement is detected 
function sketchpad_touchMove(e) {
    getTouchPos(e);
    Tempctx.clearRect(0, 0, Tempcanvas.width, Tempcanvas.height);
    Shapes(touchX,touchY,Tempctx);
    draw(touchPrevX,touchPrevY,touchX,touchY);    
    event.preventDefault();}
    
function sketchpad_touchEnd(e) {Shapes(touchX,touchY,ctx);}    

function getTouchPos(e) {
    if (!e) var e = event; if(e.touches) { 
    if (e.touches.length == 1) { 
    // Only deal with one finger
    var touch = e.touches[0]; 
    // Get the information for finger #1
    touchPrevX = touchX;
    touchPrevY = touchY;
    touchX=touch.pageX-touch.target.offsetLeft; 
    touchY=touch.pageY-touch.target.offsetTop; } } } 

// Set-up the canvas and add our event handlers after the page has loaded 
function init() {
    canvas = document.getElementById('sketchpad');
    if (canvas.getContext) {
    ctx = canvas.getContext("2d");}
        
    Tempcanvas = document.getElementById('TempSketch');
    if (Tempcanvas.getContext) {
    Tempctx = Tempcanvas.getContext("2d");
          // React to mouse events on the canvas, and mouseup on the entire document 
    Tempcanvas.addEventListener('mousedown', sketchpad_mouseDown, false);
    Tempcanvas.addEventListener('mousemove', sketchpad_mouseMove, false);
    Tempcanvas.addEventListener('mouseup', sketchpad_mouseUp, false);
     
    // React to touch events on the canvas 
    Tempcanvas.addEventListener('touchstart', sketchpad_touchStart, false); 
    Tempcanvas.addEventListener('touchmove', sketchpad_touchMove, false);
    Tempcanvas.addEventListener('touchend', sketchpad_touchEnd, false);
          }
      }
 
function IntegrImage() {
        var imag = new Image();        
        var file = document.getElementById('fichierFond').files[0];
        var reader = new FileReader();
        reader.addEventListener("load", function() {
        imag.src = reader.result;}, false);
        if (file) {reader.readAsDataURL(file);}
        imag.onload = function() { 
        var ratio = imag.width/imag.height;
        canvas.width = window.innerWidth;
        canvas.height = canvas.width/ratio;
        Tempcanvas.width = window.innerWidth;
        Tempcanvas.height = canvas.width/ratio;
        ctx.drawImage(imag,0,0,canvas.width,canvas.height);};				
					}

function SaveCanvas(){
	var link = document.createElement('a');
	link.setAttribute('download', 'Image.png');	
	canvas.toBlob(function(blob) {
    var url = URL.createObjectURL(blob);
    link.setAttribute('href', url);
    link.click();});
}

function SelectRadio(RadioName) {
	var selectR = document.getElementsByName(RadioName);
	var inputValueR = "";
	for (var i = 0; i < selectR.length; i++) {
    if (selectR[i].type === 'radio' && selectR[i].checked) {
    inputValueR = selectR[i].value;}}
	return inputValueR;	
	}

var PushArray = new Array();
var Step = -1;
	
function Push() {
    Step++;
    if (Step < PushArray.length) { PushArray.length = Step; }
    PushArray.push(canvas.toDataURL());
}

function Undo() {
    if (Step > 0) {
        Step--;
        var canvasPic = new Image();
        canvasPic.src = PushArray[Step];
        canvasPic.onload = function () {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        Tempctx.clearRect(0, 0,Tempcanvas.width,Tempcanvas.height);
        ctx.drawImage(canvasPic, 0, 0); }
    }
}

function Redo() {
    if (Step < PushArray.length-1) {
        Step++;
        var canvasPic = new Image();
        canvasPic.src = PushArray[Step];
        canvasPic.onload = function () {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        Tempctx.clearRect(0, 0,Tempcanvas.width,Tempcanvas.height);
        ctx.drawImage(canvasPic, 0, 0); }}
}